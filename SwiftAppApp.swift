//
//  SwiftAppApp.swift
//  SwiftApp
//
//  Created by Nitin Singh on 23/10/20.
//

import SwiftUI

@main
struct SwiftAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
