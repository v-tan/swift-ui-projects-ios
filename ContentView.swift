//
//  ContentView.swift
//  SwiftUIDemo
//
//  Created by Nitin Singh on 23/10/20.
//

import SwiftUI

struct ContentView: View {
  
  var body: some View {
    
    ZStack {
      
      Color(red: 174/255, green: 221/255, blue: 210/255)
        .edgesIgnoringSafeArea(.all)
      
      VStack {
        Group {
          Text("Nice to meet you, we will chat soon.")
            .fontWeight(.bold)
            .foregroundColor(Color.black)
            .multilineTextAlignment(.leading)
            .lineLimit(4)
            .padding(.top, 50.0)
            .padding(.trailing, 50)
            .frame(width: 330, height: 316, alignment: .topLeading).font(.custom("Helvetica Neue", size: 54))
          
          Group {
            Button(action: { }) {
              Text("Take me back home")
                .fontWeight(.bold)
                .padding()
                .foregroundColor(.white)
                .frame(width: 250, height: 70, alignment: .center).font(.custom("Helvetica Neue", size: 20))
            }
          }
          .background(Color.black)
          .padding(.trailing, 70.0)
          .padding(.top, 10.0)
          
        }.background(  Color(red: 174/255, green: 221/255, blue: 210/255))
        
        Color(red: 174/255, green: 221/255, blue: 210/255)
          .edgesIgnoringSafeArea(.all)
      }
      Spacer()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
