//
//  SecondContentView.swift
//  SwiftUIDemo
//
//  Created by Nitin Singh on 23/10/20.
//

import SwiftUI

struct SecondContentView: View {
  
  @State private var isClicked = false
  @State private var name: String = ""
  @State private var phoneNumaer: String = ""
  @State private var email: String = ""
  @State private var feedback: String = ""
  
  var body: some View {
    
    ZStack {
      
      Rectangle()
        .foregroundColor(  Color(red: 174/255, green: 221/255, blue: 210/255)).edgesIgnoringSafeArea(.all)
      
      Rectangle()
        .frame(width: 600, height: 900, alignment: .center)
        .foregroundColor(.white)
        .rotationEffect(Angle(degrees: 72)).edgesIgnoringSafeArea(.all)
      
      VStack(alignment: .center, spacing: 50)
      {
        HStack {
          
          Text("Hello")
            .fontWeight(.bold)
            .foregroundColor(Color.black)
            .multilineTextAlignment(.leading)
            .padding(.trailing, 240.0)
            .padding(.top, 70.0)
            .font(.custom("Helvetica Neue", size: 52
            ))
        }
        
        VStack {
          HStack.init(alignment: .center, spacing: 150 ) {
            Text("125 Nicholas Rd").font(.custom("Helvetica Neue", size: 15))
              .fontWeight(.medium)
              .multilineTextAlignment(.leading)
            
            Text("(647)237-2581").font(.custom("Helvetica Neue", size: 15))
              .fontWeight(.medium)
              .multilineTextAlignment(.leading)
          }
          
          HStack.init(alignment: .center, spacing: 150 ) {
            Text("Toronto,ON").font(.custom("Helvetica Neue", size: 15))
              .fontWeight(.medium)
              .multilineTextAlignment(.leading)
            
            Text("topman@gmail.com").font(.custom("Helvetica Neue", size: 15))
              .fontWeight(.medium)
              .multilineTextAlignment(.leading)
          }
          
          HStack.init(alignment: .center, spacing: 150 ){
            Text("B8E 7C6").font(.custom("Helvetica Neue", size: 15))
              .fontWeight(.medium)
              .multilineTextAlignment(.leading)
            
            Text("dribble.com/topman").font(.custom("Helvetica Neue", size: 15))
              .fontWeight(.medium)
              .multilineTextAlignment(.leading)
          }
        }
        
        VStack {
          Text("Name")
            .fontWeight(.medium)
            .multilineTextAlignment(.leading)
            .padding(.trailing, 320.0)
            .font(.custom("Helvetica Neue", size: 15))
          
          TextField("Enter Your Name", text: $name).frame(width: 240, height: 20, alignment: .center)
          
          Text("Phone")
            .fontWeight(.medium)
            .multilineTextAlignment(.leading)
            .padding(.top)
            .padding(.trailing, 320.0)
            .font(.custom("Helvetica Neue", size: 15))
          
          TextField("Enter PhoneNumber", text: $phoneNumaer)
            .frame(width: 200, height: 20, alignment: .center)
          
          Text("Email")
            .fontWeight(.medium)
            .multilineTextAlignment(.leading)
            .padding(.top)
            .padding(.trailing, 320.0)
            .font(.custom("Helvetica Neue", size: 15))
          
          TextField("Enter Your Email", text: $email)
            .frame(width: 200, height: 20, alignment: .center)
          
          Text("Anything else you'd like us to know?")
            .fontWeight(.medium)
            .multilineTextAlignment(.leading)
            .padding(.top)
            .padding(.trailing, 120)
            .font(.custom("Helvetica Neue", size: 15))
          
          TextField("Anything", text: $feedback).frame(width: 200, height: 20, alignment: .center)
          
          Group {
            Button(action: { }) {
              Text("Submit")
                .fontWeight(.bold)
                .padding()
                .foregroundColor(.white)
                .frame(width: 256, height: 60, alignment: .center).font(.custom("Helvetica Neue", size: 16))
            }
          }
          .background(Color.black)
          .padding(.trailing, 100.0)
          .padding(.top, 90)
        }
      }.padding()
      Spacer()
    }
  }
}

struct SecondContentView_Previews: PreviewProvider {
  static var previews: some View {
    SecondContentView()
  }
}
